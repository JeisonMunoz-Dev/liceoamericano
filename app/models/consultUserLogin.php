<?php
//Consultar usuario
class ConsultUserLogin extends Base{
    public $_SESSION;
    public function validarUsuario($Data){
        try{
            $this->consultar("SELECT * FROM usuario WHERE username = ? AND password = ? ");
            $this->bind(1,sha1($Data['name']),PDO::PARAM_STR);
            $this->bind(2,sha1($Data['password']),PDO::PARAM_STR);
            $this->execute();
            $_request = $this->registro();

            if($_request != null  ){
                
                return $_request;
            }else{
                return false;
            }
        }catch(Exception $e){
            die();
        }
    }
}
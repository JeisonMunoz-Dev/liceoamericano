<div class="container-menu">
    <div class="opt-user">
        <ul>
            <li class="btn-op"><a href="<?= RUTA_URL?>ControllerView/viewCursos">Consultar Cursos</a></li>
            <li class="btn-op"><a href="<?= RUTA_URL?>ControllerView/viewTableStudents">Consultar Estudiantes</a></li>
            <li class="btn-op"><a href="<?= RUTA_URL?>ControllerView/">Generar certificado</a></li>
            <li class="btn-op"><a href="<?= RUTA_URL?>ControllerView/">Historial certificados</a></li>
            <li class="btn-op"><a href="<?= RUTA_URL?>ControllerView/viewNewCourse">Nuevo curso</a></li>
            <li class="btn-op"><a href="<?= RUTA_URL?>ControllerView/viewNewStudent">Nuevo Estudiante</a></li>
            <li class="btn-op"><a href="<?= RUTA_URL?>ControllerView/viewNewTeacher">Nuevo Intructor</a></li>
        </ul>
    </div>
    <div class="btn-menu">
        <i class="fas fa-ellipsis-h"></i>
        <span>Menu</span>
    </div>
</div>
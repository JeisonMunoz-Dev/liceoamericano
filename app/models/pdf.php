<?php

require_once (RUTA_URL.'public/librery/fpdf/fpdf.php');
$pdf = new FPDF();
$pdf->AddPAge('PORTRAIT'.'letter');

class pdf extends FPDF{

    public function header(){

        $this->SetFont('Courier','B',12);
        $this->Write(5,'Centro Educativo Colonia La Paz');
        
    }

    public function body(){
        
            // $nombre=$data->nombre;
            // $identificacion=$data->Identificacion;
            // $grado=$data->grado;
            try{
                require_once ('librery\fpdf\WriteHTML.php');
                $pdf = new PDF();
                //Nueva Pagina en el PDF
                $pdf->AddPage('portrait','letter');
                //Imagen(ruta,posicionX,posicionY,alto,ancho,tipo,link)
                /**
                 * Cabecera del Documento
                 */
                $pdf->Image('img/logotipo.jpg',10,10,38,25,'jpg');
                $pdf->Image('img/logotipoLetra.jpg',51,13,110,7,'jpg');
                $pdf->Image('img/logotipoC.jpg',170,21,30,13,'jpg');
                $pdf->SetY(20);    
                $pdf->SetFont('Arial','',7);
                $pdf->Cell(190,3, utf8_decode('Aprobado segun Resolucion No. 2885 del 22-Nov-2005, Resolucion No. 3237 de 23-Ago-2007'),0,1,'C',false);
                $pdf->ln(0);
                $pdf->Cell(190,3, utf8_decode('Resolucion No. 0086 de 26-Feb-2010, Resolucion No. 09-115 de 16-May-2011'),0,1,'C',false);
                $pdf->ln(0);
                $pdf->Cell(190,3, utf8_decode('Resolucion No. 09-016 de 25-Abr-2013 y Resolucion No. 09-024 de 4-Jun-2013'),0,1,'C',false);
                $pdf->ln(0);
                $pdf->Cell(190,3, utf8_decode('expedidads por la Secretaria de Educación de Bogotá D.C'),0,1,'C',false);
                $pdf->ln(0);
                $pdf->Cell(190,3, utf8_decode('DANE31100110342B  CODIGO ICFES 173542'),0,1,'C',false);
                $pdf->ln(0);

                $pdf->SetY(45);    
                //Definir el tipo de fuente (T,E,TMÑ)
                //estilo [normal,B,I,U]
                $pdf->SetFont('Arial','',10);
                //agregar texto como tipo tablas
                //parametros Ancho(mm || lo que ocupa), Alto, Texto (Opcionales: bordes,?,Alineacion[C,L,R,F],rellenar,link)
                $pdf->Cell(45,4, utf8_decode('Las suscritas'),0,0,'R',false); 
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(125,4, utf8_decode(' RECTORA Y SECRETARIA DEL LICEO AMERICANO MI GRAN CASA AZUL,'),0,1,'C',false); 
                $pdf->ln(0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(190,4,utf8_decode('reconocido oficialmente por la Secretaria de Educacion de Bogotá, D.C.,'),0,1,'C',false);
                $pdf->ln(0);
                $pdf->Cell(43,4,utf8_decode('segun '),0,0,'R',false);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(83,4,utf8_decode(' Resolucion No. 4885 del 22 de Noviembre del 2005 '),0,0,'C',false);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(0,4,utf8_decode(' para los niveles de'),0,1,'L',false);
                
                $pdf->Cell(190,4,utf8_decode('preescolar y modificada por Ampliacion del servicio educativo, "EDUCACION BASICA"'),0,1,'C',false);
                $pdf->ln(0);
                $pdf->Cell(63,4,utf8_decode(' segun'),0,0,'R',false);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(5,4,utf8_decode('Resolucion No. 3437 del 23 de Agosto de 2007.'),0,1,'L',false);
                
                /**
                 * Cuerpo del Documento
                 */
                //Salto de linea
                $pdf->ln();
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(160,4,utf8_decode('Certificado No.'),0,0,'R',false);       
                $pdf->SetFont('Arial','BU',10);
                $pdf->Cell(38,4,utf8_decode('1967-17'),0,1,'L',false);
                $pdf->ln();       
                $pdf->SetFont('Arial','B',12);
                $pdf->Cell(200,4,utf8_decode('CERTIFICAN'),0,1,'C',false);
                $pdf->ln();
                $pdf->SetX(25);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(0,2,utf8_decode('Que el estudiante,  Identificado con Tajeta de identidad No. '),0,1,'F',False);
                $pdf->ln();
                $pdf->SetX(25);
                $pdf->Cell(0,2,utf8_decode('de Bogota, curso y aprobó en este Plantel el Grado  del nivel BASICA, durante el año 2014,'),0,1,'F',False);
                $pdf->ln();
                $pdf->SetX(25);
                $pdf->Cell(0,2,utf8_decode('Obteniendo los sigueintes juicios valorativos:'),0,1,'',False);




            $pdf->OutPut(); //mostrar el pdf -> Salida

            }catch(Exception $e){
                die($e);
            } 
    } 
    
    public function footer(){
        $this->SetFont('Courier','B',12);
        $this->SetY(-15);
        $this->Write(5,'San Miguel, El salvador');
    }
}

$fpdf = new pdf();
$fpdf->AddPage('portrait','letter');
$fpdf->SetFont('Arial','B',14);
$fpdf->SetY(20);
$fpdf->Cell(0,5,'Listado de estudiante Matriculados');
$fpdf->OutPut();

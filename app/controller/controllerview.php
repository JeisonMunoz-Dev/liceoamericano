<?php

class controllerView extends Controlador {
    
    public function viewCursos(){
        parent::viewH('content/consult/viewTableCourses');
    }
    public function viewLoadingFile(){
        parent::viewH('content/add/viewLoadFile');
    }
    public function viewTableStudents(){
        parent::viewH("content/consult/viewTableStudent");
    }
    public function viewNewCourse(){
        parent::viewH("content/add/view");
    }
    public function viewNewStudent(){
        parent::viewH("content/add/viewAddStudent");
    }
    public function viewNewTeacher(){
        parent::viewH("content/add/viewAddTeacher");
    }
}
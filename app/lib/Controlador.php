<?php
    //clase de controlador
    class Controlador{
        //cargar modelo
        protected $modelo;
        public function model($modelo){
            require_once("../app/models/".$modelo.".php");
            return new $modelo();

        }

        public function asignarModel($modelo){
            $this->modelo = $this->model($modelo);
        }

        public function view($vista,$datos){
            if(file_exists("../app/view/".$vista.".php")){
                require_once("../app/view/inc/header.php");
                require_once("../app/view/".$vista.".php");
                require_once("../app/view/inc/footer.php");
            }else{
                die("no existe el archivo");
            }

        }

        public function viewH($vista,$datos=[]){
            if(file_exists("../app/view/".$vista.".php")){              
                    require_once("../app/view/inc/header.php");
                    require_once("../app/view/structure/startPage.php");
                    require_once("../app/view/structure/menu.php");
                    require_once("../app/view/".$vista.".php");
                    require_once("../app/view/structure/endPage.php");
                    require_once("../app/view/inc/footer.php");
            }else{
                
            }
        } 
    }


?>
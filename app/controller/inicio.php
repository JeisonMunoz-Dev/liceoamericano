<?php

    class inicio extends Controlador{

        public $block;
        private $option;
        
        public function __construct(){    
           // setcookie('PHPUSERDATA', '', time() - 3600, '/');
            parent::asignarModel('ConsultUserLogin');   
        }
        
        public function principal(){ 
            $block = "" ;
            parent::view('inicioSesion',$block);    
        }
        
        public function valSesion(){
            $data = $this->modelo->validarUsuario($_POST);
            
            if($data == true){
                $block = "" ;
                $_SESSION = ['nombre' => $data->username];
                parent::viewH('content/consult/viewTableStudent');
            }else if($data == false){
                $block = "status400";
                parent::view('inicioSesion',$block);
            }
        }
        
        public function closesession(){
            unset($_SESSION[$_COOKIE["PHPUSERDATA"]]);
            unset($_COOKIE["PHPUSERDATA"]);
            
        }
        // parent::view("structure/startPage");
        // parent::view("structure/menu");
        // parent::view("content/tableStudent");
        // parent::view("structure/endPage");

        
      
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--====== REFERENCIA =========-->
	<link rel="shortcut icon" href="<?= RUTA_URL?>public/img/icon.png" type="image/x-icon">
	<link rel="stylesheet" href="<?= RUTA_URL?>public/css/style.css">
	<link rel="stylesheet" href="<?= RUTA_URL?>public/librery/boostrap/bootstrap-4.6.0/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Play&display=swap" rel="stylesheet">
<!--====== FIN-REFERENCIA =========-->
	<title><?=NOMBRESITIO?></title>
</head>
<body>
	

<!-- tabla donde se van a mostrar los estudiantes con su informacion -->
<div class="container-body" >
    <div class="content-table">
        
        <?php //var_dump($_SESSION)?>
        <table id="table">
            <thead>
                <tr class="headtable">
                    <th>Estudiante</th>
                    <th>Grado</th>
                    <th>Curso</th>
                    <th>Director Grupo</th>
                    <th>Año</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            
                <tr class="bodytable">
                    <td>Jeison Manuel Muñoz Rincon</td>
                    <td>Once</td>
                    <td>A</td>
                    <td>Fernel Verjel</td>
                    <td>2014</td>
                    <td><button class="btn-table-students" title="Generar PDF/Certificado del estudiante"><i class="fas fa-file-pdf"></i></button></td>
                </tr>
                <tr class="bodytable">
                    <td>Jeison Manuel Muñoz Rincon</td>
                    <td>Once</td>
                    <td>A</td>
                    <td>Fernel Verjel</td>
                    <td>2014</td>
                    <td><button class="btn-table-students" title="Generar PDF/Certificado del estudiante"><i class="fas fa-file-pdf"></i></button></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
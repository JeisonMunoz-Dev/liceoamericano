-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-08-2019 a las 06:14:07
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `liceoamericano`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `idAlumno` int(11) NOT NULL,
  `namealumno` varchar(45) DEFAULT NULL,
  `docalumno` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnohasgradohasyear`
--

CREATE TABLE `alumnohasgradohasyear` (
  `idalumnoforgradoforyear` int(11) NOT NULL,
  `year_idyear` int(11) NOT NULL,
  `Alumno_idAlumno` int(11) NOT NULL,
  `grado_idgrado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datayear`
--

CREATE TABLE `datayear` (
  `idinfo` int(11) NOT NULL,
  `alumnohasgradohasyear_idalumnoforgradoforyear` int(11) NOT NULL,
  `alumnohasgradohasyear_Alumno_idAlumno` int(11) NOT NULL,
  `alumnohasgradohasyear_year_idyear` int(11) NOT NULL,
  `alumnohasgradohasyear_grado_idgrado` int(11) NOT NULL,
  `Materia` varchar(45) NOT NULL,
  `periodo1` varchar(45) NOT NULL,
  `periodo2` varchar(45) NOT NULL,
  `periodo3` varchar(45) NOT NULL,
  `notafinal` varchar(45) NOT NULL,
  `Intructor_idIntructor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grado`
--

CREATE TABLE `grado` (
  `idgrado` int(11) NOT NULL,
  `grado` varchar(45) NOT NULL,
  `grupo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intructor`
--

CREATE TABLE `intructor` (
  `idIntructor` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUser` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUser`, `username`, `password`) VALUES
(1, 'JeisonMR98', '123456'),
(2, 'Jeison2MR98', '123456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `year`
--

CREATE TABLE `year` (
  `idyear` int(11) NOT NULL,
  `yearchar` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`idAlumno`);

--
-- Indices de la tabla `alumnohasgradohasyear`
--
ALTER TABLE `alumnohasgradohasyear`
  ADD PRIMARY KEY (`idalumnoforgradoforyear`,`year_idyear`,`Alumno_idAlumno`,`grado_idgrado`),
  ADD KEY `fk_alumnohasgradohasyear_year_idx` (`year_idyear`),
  ADD KEY `fk_alumnohasgradohasyear_Alumno1_idx` (`Alumno_idAlumno`),
  ADD KEY `fk_alumnohasgradohasyear_grado1_idx` (`grado_idgrado`);

--
-- Indices de la tabla `datayear`
--
ALTER TABLE `datayear`
  ADD PRIMARY KEY (`idinfo`,`alumnohasgradohasyear_idalumnoforgradoforyear`,`alumnohasgradohasyear_Alumno_idAlumno`,`alumnohasgradohasyear_year_idyear`,`alumnohasgradohasyear_grado_idgrado`,`Intructor_idIntructor`),
  ADD KEY `fk_datayear_alumnohasgradohasyear1_idx` (`alumnohasgradohasyear_idalumnoforgradoforyear`,`alumnohasgradohasyear_year_idyear`,`alumnohasgradohasyear_Alumno_idAlumno`,`alumnohasgradohasyear_grado_idgrado`),
  ADD KEY `fk_datayear_Intructor1_idx` (`Intructor_idIntructor`);

--
-- Indices de la tabla `grado`
--
ALTER TABLE `grado`
  ADD PRIMARY KEY (`idgrado`);

--
-- Indices de la tabla `intructor`
--
ALTER TABLE `intructor`
  ADD PRIMARY KEY (`idIntructor`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUser`);

--
-- Indices de la tabla `year`
--
ALTER TABLE `year`
  ADD PRIMARY KEY (`idyear`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumnohasgradohasyear`
--
ALTER TABLE `alumnohasgradohasyear`
  ADD CONSTRAINT `fk_alumnohasgradohasyear_Alumno1` FOREIGN KEY (`Alumno_idAlumno`) REFERENCES `alumno` (`idAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_alumnohasgradohasyear_grado1` FOREIGN KEY (`grado_idgrado`) REFERENCES `grado` (`idgrado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_alumnohasgradohasyear_year` FOREIGN KEY (`year_idyear`) REFERENCES `year` (`idyear`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `datayear`
--
ALTER TABLE `datayear`
  ADD CONSTRAINT `fk_datayear_Intructor1` FOREIGN KEY (`Intructor_idIntructor`) REFERENCES `intructor` (`idIntructor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_datayear_alumnohasgradohasyear1` FOREIGN KEY (`alumnohasgradohasyear_idalumnoforgradoforyear`,`alumnohasgradohasyear_year_idyear`,`alumnohasgradohasyear_Alumno_idAlumno`,`alumnohasgradohasyear_grado_idgrado`) REFERENCES `alumnohasgradohasyear` (`idalumnoforgradoforyear`, `year_idyear`, `Alumno_idAlumno`, `grado_idgrado`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
